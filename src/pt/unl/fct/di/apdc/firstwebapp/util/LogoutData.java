package pt.unl.fct.di.apdc.firstwebapp.util;

public class LogoutData {

	public String username;
	public String tokenID;
	
	public LogoutData(){

	}

	public LogoutData(String username, String tokenID){
		this.username = username;
		this.tokenID = tokenID;
	}
}
