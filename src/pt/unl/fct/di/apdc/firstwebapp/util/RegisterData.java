package pt.unl.fct.di.apdc.firstwebapp.util;

public class RegisterData {

	public String username;
	public String name;
	public String[] password;
	public String id;
	public String nif;
	public String email;	
	public String phone;
	public String mobile;
	public String address;
	public String complement;
	public String locality;
	public String postal;
	
	public RegisterData() {
		
	}
	
	public RegisterData(String username, String name, 
			String []password,
			 String id,String nif, String email,
			 String phone, String mobile,
			 String address, String complement,
			 String locality,String postal) {
		
			this.username = username;
			this.name = name;
			this.password = password;
			this.id = id;
			this.nif = nif;
			this.email = email;
			this.phone = phone;
			this.mobile = mobile;
			this.address = address;
			this.complement = complement;
			this.locality = locality;
			this.postal = postal;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
	public boolean validRegistration() {
		return validField(username) &&
				   validField(name) &&
			   validField(email) &&
			   validField(password[0]) &&
			   validField(password[1]) &&
			   password[0].equals(password[1]) &&
			   email.contains("@") &&
			   validField(id) &&
			   validField(nif) &&
			   validField(phone) &&
			   validField(mobile) &&
			   validField(address) &&
			   validField(complement) &&
			   validField(locality) &&
			   validField(postal);
		
	}
	
}