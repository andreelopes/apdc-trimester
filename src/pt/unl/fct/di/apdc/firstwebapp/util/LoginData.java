package pt.unl.fct.di.apdc.firstwebapp.util;

public class LoginData {

	public String identificator;
	public String password;
	
	public LoginData(){

	}
	public LoginData(String identificator, String password){
		this.identificator = identificator;
		this.password = password;
	}

}
