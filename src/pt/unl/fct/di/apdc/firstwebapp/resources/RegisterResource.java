package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import pt.unl.fct.di.apdc.firstwebapp.util.LoginData;
import pt.unl.fct.di.apdc.firstwebapp.util.RegisterData;

@Path("/register")
public class RegisterResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public RegisterResource() {
	} // Nothing to be done here...


	@POST
	@Path("/v1")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doRegistrationV3(RegisterData data) {

		if( ! data.validRegistration() ) {
			//System.out.println("Bla1");

			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.username);
			Entity user = datastore.get(userKey);

			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 

		} catch (EntityNotFoundException e) {

			Query ctrQuery = new Query("User").setFilter(new FilterPredicate("user_id", FilterOperator.EQUAL, Integer.parseInt(data.id)));
			List<Entity> results1 = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

			if(!results1.isEmpty()){
				return Response.status(Status.BAD_REQUEST).entity("id already exists").build();
			}
			Query ctrQuery2 = new Query("User").setFilter(new FilterPredicate("user_nif", FilterOperator.EQUAL, data.nif));
			List<Entity> results2 = datastore.prepare(ctrQuery2).asList(FetchOptions.Builder.withDefaults());

			
			if(!results2.isEmpty()){
				return Response.status(Status.BAD_REQUEST).entity("nif already exists").build();
			}
			
			Query ctrQuery3 = new Query("User").setFilter(new FilterPredicate("user_email", FilterOperator.EQUAL, data.email));
			List<Entity> results3 = datastore.prepare(ctrQuery3).asList(FetchOptions.Builder.withDefaults());


			if(!results3.isEmpty()){
				return Response.status(Status.BAD_REQUEST).entity("email already exists").build();
			}

			Entity user = new Entity("User", data.username);

			user.setProperty("user_name", data.name);
			user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password[0]));
			user.setProperty("user_email", data.email);
			user.setUnindexedProperty("user_creation_time", new Date());
			user.setProperty("user_id", Integer.parseInt(data.id));
			user.setProperty("user_nif", Integer.parseInt(data.nif));
			user.setProperty("user_phone", data.phone);
			user.setProperty("user_mobile", data.mobile);
			user.setProperty("user_address", data.address);
			user.setProperty("user_complement", data.complement);
			user.setProperty("user_locality", data.locality);
			user.setProperty("user_postal", data.postal);


			datastore.put(txn,user);
			LOG.info("User registered " + data.username);
			txn.commit();
			return Response.ok().build();


		}	catch (Exception e2) {
			LOG.info("SHIT");
			return Response.status(Status.BAD_REQUEST).entity("unknown shit").build(); 


		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}
}