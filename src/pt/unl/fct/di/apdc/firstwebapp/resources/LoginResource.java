package pt.unl.fct.di.apdc.firstwebapp.resources;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javassist.compiler.TokenId;
import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;

@Path("/login")
public class LoginResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();

	public LoginResource() { } //Nothing to be done here...

	@POST
	@Path("/v1")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginV2(pt.unl.fct.di.apdc.firstwebapp.util.LoginData data) {
		LOG.fine("Attempt to login user: " + data.identificator);

		Transaction txn = datastore.beginTransaction();
	
		
		try {
			Key userKey = KeyFactory.createKey("User", data.identificator);
			Entity user = datastore.get(userKey);

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				// Password correct

				// Return token
				AuthToken token = new AuthToken(user.getKey().getName());
				LOG.info("User " + user.getKey().getName() + "' logged in sucessfully.");


				Entity tokenEn = new Entity("Token", token.tokenID, user.getKey());
				tokenEn.setProperty("expiration", token.expirationData);
				tokenEn.setProperty("creaton", token.creationData);
				tokenEn.setProperty("actived", true);

				datastore.put(txn,tokenEn);

				txn.commit();
				
				return Response.ok(g.toJson(token)).build();				
			} else {
				// Incorrect password
				txn.rollback();

				LOG.warning("Wrong password"+ user.getKey().getName());
				return Response.status(Status.BAD_REQUEST).build();				
			}

		}catch(EntityNotFoundException e){

			txn = datastore.beginTransaction();
			
			Entity user = null;

			List<Entity> results1 = null;
			List<Entity> results3 = null;
			List<Entity> results2 = null;

			Query ctrQuery = new Query("User").setFilter(new FilterPredicate("user_id", FilterOperator.EQUAL, Integer.parseInt(data.identificator)));
			results1 = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			if(results1.isEmpty()){
				Query ctrQuery2 = new Query("User").setFilter(new FilterPredicate("user_nif", FilterOperator.EQUAL, data.identificator));
				results2 = datastore.prepare(ctrQuery2).asList(FetchOptions.Builder.withDefaults());

				if(results2.isEmpty()){
					Query ctrQuery3 = new Query("User").setFilter(new FilterPredicate("user_email", FilterOperator.EQUAL, data.identificator));
					results3 = datastore.prepare(ctrQuery3).asList(FetchOptions.Builder.withDefaults());	
					if(!results3.isEmpty()) user = results3.get(0);
				}
				else{
					user = results2.get(0);
				}
			}else{
				user = results1.get(0);
			}

			if(results1.isEmpty() && results2.isEmpty() && results3.isEmpty()){
				txn.rollback();
				return Response.status(Status.BAD_REQUEST).entity("Identificator not valid").build();
			}

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				// Password correct

				// Return token
				AuthToken token = new AuthToken(user.getKey().getName());
				LOG.info("User " + user.getKey().getName() + " logged in sucessfully.");
				
				
				Key userKey = KeyFactory.createKey("User", user.getKey().getName());
				
				try {
					user = datastore.get(userKey);
				} catch (EntityNotFoundException e1) {
					e1.printStackTrace();
				}
				
				Entity tokenEn = new Entity("Token", token.tokenID, user.getKey());
				tokenEn.setProperty("expiration", token.expirationData);
				tokenEn.setProperty("creaton", token.creationData);
				tokenEn.setProperty("actived", true);
	
				
				datastore.put(txn,tokenEn);


				txn.commit();

				return Response.ok(g.toJson(token)).build();				
			} else {
				// Incorrect password
				txn.rollback();

				LOG.warning("Wrong password"+ user.getKey().getName());
				return Response.status(Status.BAD_REQUEST).build();				
			}

		}
//		} catch (Exception e) {
//			LOG.warning("Failed login");
//			txn.rollback();
//			return Response.status(Status.FORBIDDEN).entity("Username not valid").build();
//		}
		finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}


}