package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/map")
public class MapResource {

	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();


	public MapResource(){}

	@GET
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUserLocality(@QueryParam("tkn") String tokenId) {
		
		try{
			Key tknkey = KeyFactory.createKey("Token", tokenId);
			Entity token = datastore.get(tknkey);
			
			Key userKey = KeyFactory.createKey("User", (String) token.getProperty("username"));
			Entity user = datastore.get(userKey);
			
			String locality = (String) user.getProperty("user_locality");
			
			return Response.ok(g.toJson(locality)).build();

		}catch(Exception e){
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

}
