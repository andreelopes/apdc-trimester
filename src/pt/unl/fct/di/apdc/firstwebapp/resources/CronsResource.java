package pt.unl.fct.di.apdc.firstwebapp.resources;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;


@Path("/crons")
public class CronsResource {

	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();

	@GET
	@Path("/tokenBye")
	@Produces(MediaType.APPLICATION_JSON)
	public Response clearOutdatedTokens(){


		try {

			Query ctrQuery = new Query("Token").setFilter(new FilterPredicate("actived", FilterOperator.EQUAL, true));
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

			List<Entity> bla = new ArrayList<>();


			for (int i = 0; i < results.size(); i+=24) {
				
				Transaction txn = datastore.beginTransaction();

				//apenas da para por 25 entidades duma vez
				for (int j = i; j < i + 24 && j < results.size(); j++) {

					Entity e = results.get(j);

					long expirationTime = (long) e.getProperty("expiration");


					if(expirationTime <= System.currentTimeMillis())
						e.setProperty("actived", false);

					bla.add(e);
				}
				
				datastore.put(bla);
				txn.commit();
				bla.clear();
			}

			return Response.ok().build();				
		} 
		catch (Exception e) {
			LOG.warning("Cron falhou");
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}
