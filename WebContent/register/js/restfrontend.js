captureData = function(event) {
	var data = $('form[name="register"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "http://myfirstwebsitenew.appspot.com/rest/register/v1",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {

			location.href = "../login/loginv2.html";

		},
		error: function(response) {
			if(response.status == 400){
				alert(response.responseText);
				location.reload();
			}
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	var frms = $('form[name="register"]');   
	frms[0].onsubmit = captureData;
}