var map;

function initMap() 
{	



	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat:  38.659784, lng:  -9.202765},
		zoom: 16
	});
	
	var marker = searchAddress();


	var contentString = '<div id="content">'+
	'<h1 id="title">User is here</h1>'+
	'</div>';

	var infowindow = new google.maps.InfoWindow({content: contentString});

	marker.addListener('click', function() {
		infowindow.open(map, marker);
	});
}


function searchAddress() {

	var addressInput = getAdress();
	var marker;

	var geocoder = new google.maps.Geocoder();

	geocoder.geocode({address: addressInput}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {

			var myResult = results[0].geometry.location; // referência ao valor LatLng

			marker = createMarker(myResult); // adicionar chamada à função que adiciona o marcador

			map.setCenter(myResult);

			map.setZoom(17);
		}
	});
	return marker;
}

function createMarker(latlng) {

	marker = new google.maps.Marker({
		map: map,
		position: latlng
	});

}

function tokenValid(){

	var token = localStorage.getItem('tokenID');
	var ret = null;
	if(token != null){
		$.ajax({
			type: "GET",
			url: "http://myfirstwebsitenew.appspot.com/rest/token/valid?tkn=" + token,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response) {
				if(response)
					ret = true;
				else 
					ret = false;
			},
			data: JSON.stringify(data)
		});
	}

	return ret;
};

function getAdress(){


	var token = localStorage.getItem('tokenID');
	if(token != null){

		var ret = null;

		$.ajax({
			type: "GET",
			url: "http://myfirstwebsitenew.appspot.com/rest/map/user?tkn=" + token,
			contentType: "application/json; charset=utf-8",
			crossDomain: true,
			success: function(response) {
				ret = response.locality;
			},
			data: JSON.stringify(data)
		});
	}else
		return "Tomar";

	return ret;
};






